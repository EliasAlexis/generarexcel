
from bd import conexion
import xlsxwriter
import shutil

try:
    with conexion.cursor() as cursor:
        cursor.execute("SELECT rut  FROM tbl_pivot_total_2019 where  (YEAR(FEC_CASTIT) = 2019) AND (CARTASCERT>0);")

        # Con fetchall traemos todas las filas
        ruts = cursor.fetchall()
        for rut in ruts:
            rut = str(rut[0])
            print(rut)
            rutLenSdv = len(rut)-1
            rutLenCdv = len(rut)-0
            rutCdv =  rut[0:rutLenCdv] 
            rutSdv = rut[0:rutLenSdv]       
            # print(rutCdv)
            # print("rutSdv "+str(rutSdv))
            nameExcel = "CartasCertifica.xlsx"
            
            try:
                with conexion.cursor() as cursorFono: 
                    cursorFono.execute("SELECT RUT, CODIGO_ENVIO, OT, PROYECTO, REFERENCIA, NOMBRE, DIRECCION, ESTADO, SUB_ESTADO, FECHA_ADMISION, FECHA_ENTREGA, COMUNA, SECTOR, CUARTEL, SERVICIO FROM CartasCertificas2019 where rut='"+ str(rutSdv) +"';") 
                    ges = cursorFono.fetchall()
                    # print(ges)

                    # Create a workbook and add a worksheet.
                    workbook = xlsxwriter.Workbook(str(nameExcel))
                    worksheet = workbook.add_worksheet()

                    # Start from the first cell. Rows and columns are zero indexed.
                    row = 0
                    col = 0
                    # Iterate over the data and write it out row by row.
                    worksheet.write(row, col, "RUT")
                    worksheet.write(row, col + 1, "CODIGO_ENVIO" )
                    worksheet.write(row, col + 2, "OT")
                    worksheet.write(row, col + 3, "PROYECTO")
                    worksheet.write(row, col + 4, "REFERENCIA")
                    worksheet.write(row, col + 5, "NOMBRE")
                    worksheet.write(row, col + 6, "DIRECCION")
                    worksheet.write(row, col + 7, "ESTADO")
                    worksheet.write(row, col + 8, "SUB_ESTADO")
                    worksheet.write(row, col + 9, "FECHA_ADMISION")
                    worksheet.write(row, col + 10, "FECHA_ENTREGA")
                    worksheet.write(row, col + 11, "COMUNA")
                    worksheet.write(row, col + 12, "SECTOR")
                    worksheet.write(row, col + 13, "CUARTEL")
                    worksheet.write(row, col + 14, "SERVICIO")
                    row = 1

                    for   ge in (ges):
                        worksheet.write(row, col, ge[0])
                        worksheet.write(row, col + 1, ge[1])
                        worksheet.write(row, col + 2, ge[2])
                        worksheet.write(row, col + 3, ge[3])
                        worksheet.write(row, col + 4, ge[4])
                        worksheet.write(row, col + 5, ge[5])
                        worksheet.write(row, col + 6, ge[6])
                        worksheet.write(row, col + 7, ge[7])
                        worksheet.write(row, col + 8, ge[8])
                        worksheet.write(row, col + 9, ge[9])
                        worksheet.write(row, col + 10, ge[10])
                        worksheet.write(row, col + 11, ge[11])
                        worksheet.write(row, col + 12, ge[12])
                        worksheet.write(row, col + 13, ge[13])
                        worksheet.write(row, col + 14, ge[14])
                        row += 1
                    workbook.close()
                    shutil.move(nameExcel, "E:/Castigo2019/"+str(rutCdv)+"/"+str(nameExcel))
                
                    with conexion.cursor() as cursor:
                        cursor.execute("update tbl_pivot_total_2019 set marca='ok' where  (YEAR(FEC_CASTIT) = 2019) and rut='"+str(rutCdv)+"' ;")
            
            except Exception as e:
                print("Ocurrió un error al consultar Gestiones: ", e)
            # finally:
            #     conexion.close()

except Exception as e:
    print("Ocurrió un error al consultar: ", e)
finally:
    conexion.close()


